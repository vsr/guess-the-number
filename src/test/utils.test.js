var utils = require('../js/utils');
var expect = require('chai').expect;

describe('Test utils functions:', function(){

    describe('fibonacci function', function(){

        it('Should return 0 for numbers <= 0', function(){
            expect(utils.fibonacci(0)).to.be.equal(0);
            expect(utils.fibonacci(-1)).to.be.equal(0);
        });

        it('Should return correct fibonacci number', function(){
            expect(utils.fibonacci(1)).to.be.equal(1);
            expect(utils.fibonacci(2)).to.be.equal(1);
            expect(utils.fibonacci(3)).to.be.equal(2);
            expect(utils.fibonacci(5)).to.be.equal(5);
            expect(utils.fibonacci(6)).to.be.equal(8);
            expect(utils.fibonacci(20)).to.be.equal(6765);
        });

    });


    describe('getRandomInt function', function(){

        it('Should return a number', function(){
            expect(utils.getRandomInt(1, 3)).to.be.a('number');
        });

        it('Should return random number within given range', function(){
            expect(utils.getRandomInt(1, 3)).to.be.within(1, 3);
            for(var i=0;i<300;i++){
                expect(utils.getRandomInt(1, 257)).to.be.within(1, 257);
            }
        });

    });


    describe('extend function', function(){

      it('should return an object', function(){
        expect(utils.extend({}, {})).to.be.an('object');
      });

      it('should return non-empty object', function(){
        expect(utils.extend({}, {'key': 'value'})).to.not.be.empty;
        expect(utils.extend({'key': 'value'}, {})).to.not.be.empty;
      });

      it('should return empty object', function(){
        expect(utils.extend({}, {})).to.be.empty;
      });

      it('should return object with source and destination keys', function(){
        expect(utils.extend({}, {'key': 'value', 'key2': 'value2'})).to.have.keys(['key', 'key2']);
        expect(utils.extend({'key': 'value', 'key2': 'value2'}, {})).to.have.keys(['key', 'key2']);
        expect(utils.extend({'key': 'value'}, {'key2': 'value2'})).to.have.keys(['key', 'key2']);
      });

      it('should not copy parent/prototype keys', function(){
        var source = new Date();
        expect(utils.extend({}, source)).to.be.empty;
      });

    });

});