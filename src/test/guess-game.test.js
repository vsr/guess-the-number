var utils = require('../js/utils');
var GuessGame = require('../js/guess-game');
var expect = require('chai').expect;
var sinon = require('sinon');

describe('Testing GuessGame module', function(){
    var getRandomInt = utils.getRandomInt;

    after(function(){
        utils.getRandomInt = getRandomInt;
    });

    describe('Test GuessGame constructor', function(){

        it('Should return a GuessGame object', function(){
            expect(new GuessGame(1, 10)).to.be.an.instanceof(GuessGame);
        });

        it('game object should have required methods and properties', function(){
           expect(new GuessGame(1, 10)).to.have.all.keys(['guess', 'getGuessCount', 'getGuesses',
                'giveUp', 'getRange', 'isSolved', 'isInProgress', 'optimumAttempts']);
        });

    });

    describe('Test getRange', function(){

        it('should return range array', function(){
            var game = new GuessGame(1, 10);
            expect(game.getRange()).to.be.an('array');
            expect(game.getRange()).to.have.lengthOf(2);
        });

        it('should return correct range', function(){
            var game = new GuessGame(1, 10);
            expect(game.getRange()).to.include.members([1, 10]);
            game = new GuessGame(100, 242);
            expect(game.getRange()).to.include.members([100, 242]);
        });

    });

    describe('Test optimumAttempts', function(){

        it('should return an integer', function(){
            var game = new GuessGame(1, 10);
            expect(game.optimumAttempts()).to.be.a('number');
        });

        it('should be number within range', function(){
            var game = new GuessGame(1, 10);
            expect(game.optimumAttempts()).to.be.within(1, 10);
        });

    });

    describe('Test getGuesses', function(){

        it('should return an array', function(){
            var game = new GuessGame(1, 10);
            expect(game.getGuesses()).to.be.an('array');
        });

        it('should return guessed data', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 10);
            game.guess(1);
            expect(game.getGuesses()).to.have.lengthOf(1);
            expect(game.getGuesses()).to.include.members([1]);

            game.guess(3);
            expect(game.getGuesses()).to.have.lengthOf(2);
            expect(game.getGuesses()).to.include.members([1, 3]);
        });

    });

    describe('Test getGuessCount', function(){

        it('should return an integer', function(){
            var game = new GuessGame(1, 10);
            expect(game.getGuessCount()).to.be.a('number');
        });

        it('should return guessed count', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 10);
            game.guess(1);
            expect(game.getGuessCount()).to.be.equal(1);

            game.guess(3);
            expect(game.getGuessCount()).to.be.equal(2);
        });

    });

    describe('Test isSolved', function(){

        it('should return boolean value', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 5);
            expect(game.isSolved()).to.be.a('boolean');
            game.guess(5);
            expect(game.isSolved()).to.be.a('boolean');
        });

        it('should return false when unsolved', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 5);
            expect(game.isSolved()).to.be.equal(false);
            game.guess(1);
            expect(game.isSolved()).to.be.equal(false);
        });

        it('should return true when solved', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 15);
            game.guess(5);
            expect(game.isSolved()).to.be.equal(true);
        });

    });

    describe('Test isInProgress', function(){

        it('should return boolean value', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 5);
            expect(game.isInProgress()).to.be.a('boolean');
            game.guess(5);
            expect(game.isInProgress()).to.be.a('boolean');
        });

        it('should return true when in progress', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 5);
            expect(game.isInProgress()).to.be.equal(true);
            game.guess(1);
            expect(game.isInProgress()).to.be.equal(true);
        });

        it('should return false when user gives up', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 5);
            expect(game.isInProgress()).to.be.equal(true);
            game.giveUp();
            expect(game.isInProgress()).to.be.equal(false);
        });

        it('should return false when user guesses correct', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 5);
            expect(game.isInProgress()).to.be.equal(true);
            game.guess(5);
            expect(game.isInProgress()).to.be.equal(false);
        });

    });

    describe('Test giveUp', function(){

        it('should return correct number', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 10);
            expect(game.giveUp()).to.be.a('number');
            expect(game.giveUp()).to.be.equal(5);
        });

        it('should set progress and solved to false', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 10);
            game.giveUp();
            expect(game.isInProgress()).to.be.equal(false);
            expect(game.isSolved()).to.be.equal(false);
        });

    });

    describe('Test guess method', function(){

        it('should throw error when number is not passed', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 10);
            var fn = function(){ game.guess('hello'); };
            expect(fn).to.throw(Error, /need to guess a number/);
        });

        it('should throw error when game is over', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 10);
            game.giveUp();
            var fn = function(){ game.guess(2); };
            expect(fn).to.throw(Error, /Game is over/);
        });

        it('should return 0 when guess is correct', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 10);
            expect(game.guess(5)).to.be.equal(0);
        });

        it('should return -1 when guess is lower than number', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 10);
            expect(game.guess(2)).to.be.equal(-1);
        });

        it('should return 1 when guess is greater than number', function(){
            utils.getRandomInt = sinon.stub().returns(5);
            var game = new GuessGame(1, 10);
            expect(game.guess(7)).to.be.equal(1);
        });


    });


});