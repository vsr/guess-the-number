var utils = require('../js/utils');
var GuessGame = require('../js/guess-game');
var GuessGameManager = require('../js/guess-game-manager');
var expect = require('chai').expect;
var sinon = require('sinon');

describe('Test GuessGameManager module', function(){
    var getRandomInt = utils.getRandomInt;

    after(function(){
        utils.getRandomInt = getRandomInt;
    });

    describe('GuessGameManager constructor', function(){

        it('should create an instance of GuessGameManager', function(){
            expect(new GuessGameManager()).to.be.an.instanceOf(GuessGameManager);
        });

        it('should have following keys', function(){
            expect(new GuessGameManager()).to.have.all.keys(['getGame', 'replayGame', 'moveNextLevel', 'getRange', 'getGames', 'getLevel']);
        });
    });

    describe('getGame method', function(){

        it('should return a guess-game instance', function(){
            var ggm = new GuessGameManager(1);
            expect(ggm.getGame()).to.be.an.instanceOf(GuessGame);
        });

    });

    describe('replayGame method', function(){

        it('should return a guess-game instance', function(){
            var ggm = new GuessGameManager(1);
            ggm.getGame();
            expect(ggm.replayGame()).to.be.an.instanceOf(GuessGame);
        });

        it('should return a new game', function(){
            var ggm = new GuessGameManager(1);
            expect(ggm.getGame()).to.not.be.equal(ggm.replayGame());
        });

        it('should increment games list', function(){
            var ggm = new GuessGameManager(1);
            ggm.getGame();
            ggm.replayGame();
            expect(ggm.getGames()).to.have.lengthOf(2);
            expect(ggm.getGames()[0]).to.be.an.instanceOf(GuessGame);
        });

    });

    describe('getRange method', function(){

        it('should return an array', function(){
            var ggm = new GuessGameManager(1);
            expect(ggm.getRange()).to.be.an('array');
        });

        it('should return range numbers', function(){
            var ggm = new GuessGameManager(2);
            expect(ggm.getRange()).to.include.members([1, 5]);
        });

    });

    describe('moveNextLevel method', function(){

        it('should throw Error when game is not solved', function(){
            var ggm = new GuessGameManager(1);
            ggm.getGame();
            var fn = function(){ ggm.moveNextLevel(); };
            expect(fn).to.throw(Error, /Solve current level to move to next level/);
            ggm.getGame().giveUp();
            expect(fn).to.throw(Error, /Solve current level to move to next level/);
        });

        it('should create new game when previous game is solved', function(){
            var number = 3;
            var ggm = new GuessGameManager(1);
            utils.getRandomInt = sinon.stub().returns(number);
            var firstGame = ggm.getGame();
            expect(ggm.getGames()).to.have.lengthOf(1);
            ggm.getGame().guess(number);
            ggm.moveNextLevel();
            expect(ggm.getGame()).to.not.be.equal(firstGame);
            expect(ggm.getGames()).to.have.lengthOf(2);
        });
    });

});