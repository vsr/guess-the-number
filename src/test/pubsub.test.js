var PubSub = require('../js/pubsub');
var expect = require('chai').expect;
var sinon = require('sinon');


describe('Test PubSub module', function(){

    describe('PubSub constructor', function(){

        it('Should return a PubSub object', function(){
            expect(new PubSub()).to.be.an('object');
            expect(new PubSub()).to.be.an.instanceOf(PubSub);
        });

        it('Should set pubsub name', function(){
            expect(new PubSub('pub').name).to.be.equal('pub');
            expect(new PubSub().name).to.be.equal('');
        });

        it('Should inherit subscribe, unsubscribeAll and publish methods', function(){
            expect(new PubSub()).to.respondTo('subscribe');
            expect(new PubSub()).to.respondTo('unsubscribeAll');
            expect(new PubSub()).to.respondTo('publish');
        });

    });

    describe('subscribe method', function(){

        it('Should subscribe to an event by updating events', function(){
            var pubsub = new PubSub();
            var eventNameA = 'new-event',
                eventNameB = 'new-event2',
                cbB = function(){};
            pubsub.subscribe(eventNameA, function(){});
            pubsub.subscribe(eventNameB, cbB);
            pubsub.subscribe(eventNameA, function(){});
            expect(pubsub._events[eventNameA].length).to.be.equal(2);
            expect(pubsub._events[eventNameB].length).to.be.equal(1);
            expect(pubsub._events[eventNameB][0]).to.be.equal(cbB);
        });

        it('Should return function for unsubscribe', function(){
            var pubsub = new PubSub();
            expect(pubsub.subscribe('event',function(){})).to.be.a('function');
        });

        it('Should unsubscribe on calling unsubscribe function', function(){
            var pubsub = new PubSub();
            pubsub.subscribe('event',function(){})();
            expect(pubsub._events['event'].length).to.be.equal(0);
        });

    });

    describe('unsubscribeAll method', function(){

        it('Should remove all subscribed event listeners', function(){
            var pubsub = new PubSub();
            var eventNameA = 'new-event',
                eventNameB = 'new-event2',
                cbB = function(){};
            pubsub.subscribe(eventNameA, function(){});
            pubsub.subscribe(eventNameB, cbB);
            pubsub.subscribe(eventNameA, function(){});
            pubsub.subscribe(eventNameB, function(){});
            pubsub.unsubscribeAll(eventNameA);
            expect(pubsub._events[eventNameA].length).to.be.equal(0);
            expect(pubsub._events[eventNameB].length).to.be.equal(2);
            expect(pubsub._events[eventNameB][0]).to.be.equal(cbB);
        });

    });


    describe('publish method', function(){

        it('should trigger all subscibed listeners for published event', function(){
            var pubsub = new PubSub(),
                cricket = sinon.spy(),
                football = sinon.spy(),
                sportsEvent = 'sportsEvent',
                data = {'message': 'called'};
            pubsub.subscribe(sportsEvent, cricket);
            pubsub.subscribe(sportsEvent, football);

            pubsub.publish(sportsEvent, data);

            expect(cricket.called).to.be.equal(true);
            expect(cricket.calledWith(sportsEvent, data)).to.be.equal(true);
            expect(football.called).to.be.equal(true);
            expect(football.calledWith(sportsEvent, data)).to.be.equal(true);
        });

        it('should not trigger for different events', function(){
            var pubsub = new PubSub(),
                cricket = sinon.spy(),
                politics = sinon.spy(),
                sportsEvent = 'sportsEvent',
                politicsEvent = 'PoliticsEvent',
                data = {'message': 'called'};
            pubsub.subscribe(sportsEvent, cricket);
            pubsub.subscribe(politicsEvent, politics);

            pubsub.publish(sportsEvent, data);

            expect(cricket.called).to.be.equal(true);
            expect(cricket.calledWith(sportsEvent, data)).to.be.equal(true);

            expect(politics.called).to.be.equal(false);
        });

        it('should not trigger after unsubscribing', function(){
            var pubsub = new PubSub(),
                cricket = sinon.spy(),
                sportsEvent = 'sportsEvent',
                data = {'message': 'called'};
            var unsubscribe = pubsub.subscribe(sportsEvent, cricket);
            unsubscribe();
            pubsub.publish(sportsEvent, data);

            expect(cricket.called).to.be.equal(false);

        });



    });

});