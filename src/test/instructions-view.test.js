var fs = require('fs');
var expect = require('chai').expect;
var sinon = require('sinon');
var jsdom = require('jsdom-global');
var requireHooks = require('require-hooks');

var viewUtils = require('../js/view-utils');
var PubSub = require('../js/pubsub');

requireHooks(function(hook){
    var ext = hook.ext,
        rawPath = hook.rawPath;
    switch(ext){
        case '.css':
            return '';
        case '.js':
            return fs.readFileSync(rawPath).toString();
        case '.html':
            return fs.readFileSync(rawPath).toString();
    }
});

describe('Test InstructionsView', function(){

    var InstructionsView = require('../js/instructions-view');
    var jsdomCleanup;

    beforeEach(function(){
        this.jsdomCleanup = jsdom();
    });
    afterEach(function(){
        this.jsdomCleanup();
    });


    describe('Test constructor', function(){

        it('should create an instance', function(){
            expect(new InstructionsView()).to.be.an.instanceOf(InstructionsView);
        });

        it('should inherit view util fns', function(){
            expect(InstructionsView.prototype).to.contain.keys(Object.keys(viewUtils));
        });

        it('should inherit PubSub', function(){
            expect(InstructionsView.prototype).to.contain.keys(Object.keys(PubSub.prototype));
        });

        it('should have a dom element', function(){
            var view = new InstructionsView();
            expect(view.el).to.be.an('object');
            expect(view.el.innerHTML).to.match(/back-to-game/);
            expect(view.el.classList.contains('page')).to.be.true;
            expect(view.el.classList.contains('instructions-view')).to.be.true;
        });
    });

    describe('Test events published by view', function(){

        it('should publish back-to-game on click', function(){
            var view = new InstructionsView();
            var cb = sinon.spy();
            view.subscribe('back-to-game', cb);
            view.el.querySelector('button.back-to-game').click();
            expect(cb.called).to.be.true;
            expect(cb.calledWith('back-to-game')).to.be.true;
        });

    });

});