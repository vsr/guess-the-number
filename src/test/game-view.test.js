var fs = require('fs');
var expect = require('chai').expect;
var sinon = require('sinon');
var jsdom = require('jsdom-global');
var requireHooks = require('require-hooks');

var viewUtils = require('../js/view-utils');
var PubSub = require('../js/pubsub');

requireHooks(function(hook){
    var ext = hook.ext,
        rawPath = hook.rawPath;
    switch(ext){
        case '.css':
            return '';
        case '.js':
            return fs.readFileSync(rawPath).toString();
        case '.html':
            return fs.readFileSync(rawPath).toString();
    }
});


describe('Test GameView', function(){

    var GameView = require('../js/game-view');
    var jsdomCleanup;

    beforeEach(function(){
        this.jsdomCleanup = jsdom();
    });
    afterEach(function(){
        this.jsdomCleanup();
    });

    describe('Test constructor', function(){

        it('should create an instance', function(){
            expect(new GameView()).to.be.an.instanceOf(GameView);
        });

        it('should inherit view util fns', function(){
            expect(GameView.prototype).to.contain.keys(Object.keys(viewUtils));
        });

        it('should inherit PubSub', function(){
            expect(GameView.prototype).to.contain.keys(Object.keys(PubSub.prototype));
        });

        it('should have a dom element', function(){
            var view = new GameView();
            expect(view.el).to.be.an('object');
            expect(view.el.innerHTML).to.match(/game-init-options/);
            expect(view.el.innerHTML).to.match(/guess-form/);
            expect(view.el.innerHTML).to.match(/game-off-options/);
            expect(view.el.classList.contains('page')).to.be.true;
            expect(view.el.classList.contains('game-view')).to.be.true;
        });

    });

    describe('updateScore method', function(){

        it('should update input element min. max attributes', function(){
            var view = new GameView();
            var game = {
                getRange: sinon.stub().returns([1, 5]),
                getGuessCount: sinon.stub().returns(2),
                optimumAttempts: sinon.stub().returns(3)
            };
            view.updateScore(game);
            var input = view.el.querySelector('input[name="guess-number"]');
            expect(input.getAttribute('min')).to.be.equal('1');
            expect(input.getAttribute('max')).to.be.equal('5');
        });

        it('should update range info', function(){
            var view = new GameView();
            var game = {
                getRange: sinon.stub().returns([1, 5]),
                getGuessCount: sinon.stub().returns(2),
                optimumAttempts: sinon.stub().returns(3)
            };
            view.updateScore(game);
            var el = view.el.querySelector('.range-info__value');
            expect(el.innerHTML).to.match(/1 - 5/);
        });

        it('should update attempt info', function(){
            var view = new GameView();
            var game = {
                getRange: sinon.stub().returns([1, 5]),
                getGuessCount: sinon.stub().returns(2),
                optimumAttempts: sinon.stub().returns(3)
            };
            view.updateScore(game);
            var el = view.el.querySelector('.attempt-info__value');
            expect(el.innerHTML).to.match(/2\/3/);
        });

    });

    describe('showMessage method', function(){

        it('should set message container html', function(){
            var view = new GameView();
            view.showMessage('my-message-text');
            expect(view.el.querySelector('.guess-message').innerHTML).to.match(/my-message-text/);
        });

    });

    describe('updateInput method', function(){

        it('should set input value', function(){
            var view = new GameView();
            view.updateInput('23');
            expect(view.el.querySelector('input[name="guess-number"]').value).to.equal('23');
        });

    });

    describe('published events', function (){

        it('should publish `show-instructions`', function () {
            var view = new GameView();
            var cb = sinon.spy();
            view.subscribe('show-instructions', cb);
            view.el.querySelector('.show-instructions').click();
            expect(cb.called).to.be.true;
            expect(cb.calledWith('show-instructions')).to.be.true;
        });

        it('should publish `play-start`', function () {
            var view = new GameView();
            var cb = sinon.spy();
            view.subscribe('play-start', cb);
            view.el.querySelector('button[name="play-start"]').click();
            expect(cb.called).to.be.true;
            expect(cb.calledWith('play-start')).to.be.true;
        });

        it('should publish `guess-form-submit`', function () {
            var view = new GameView();
            var cb = sinon.spy();
            view.subscribe('guess-form-submit', cb);
            view.updateInput('23');
            view.el.querySelector('form[name="guess-form"] button[type="submit"]').click();
            expect(cb.called).to.be.true;
            expect(cb.calledWith('guess-form-submit', 23)).to.be.true;
        });

        it('should publish `play-again`', function () {
            var view = new GameView();
            var cb = sinon.spy();
            view.subscribe('play-again', cb);
            view.el.querySelector('button[name="play-again"]').click();
            expect(cb.called).to.be.true;
            expect(cb.calledWith('play-again')).to.be.true;
        });

        it('should publish `play-next`', function () {
            var view = new GameView();
            var cb = sinon.spy();
            view.subscribe('play-next', cb);
            view.el.querySelector('button[name="play-next"]').click();
            expect(cb.called).to.be.true;
            expect(cb.calledWith('play-next')).to.be.true;
        });

        it('should publish `give-up`', function () {
            var view = new GameView();
            var cb = sinon.spy();
            view.subscribe('play-giveup', cb);
            view.el.querySelector('button[name="give-up"]').click();
            expect(cb.called).to.be.true;
            expect(cb.calledWith('play-giveup')).to.be.true;
        });

    });

});