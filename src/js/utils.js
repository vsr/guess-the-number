
var fibonacci = (function (i){
  "use strict";
  var memo = {
    0: 0,
    1: 1
  };
  return function(i){
    if(i<0){ i = 0; }

    if(typeof memo[i] == "undefined"){
      memo[i] = fibonacci(i-1) + fibonacci(i-2);
    }
    return memo[i];
  };

}());

var getRandomIntInclusive = function(min, max) {
  "use strict";
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

var extend = function(destination, source){
  for(var key in source){
    if(source.hasOwnProperty(key)){
      destination[key] = source[key];
    }
  }
  return destination;
};

var ga = function(){
  if(typeof window.ga === 'function'){
    ga.apply(window, arguments);
  }
};

module.exports = {
  getRandomInt: getRandomIntInclusive,
  fibonacci: fibonacci,
  extend: extend,
  ga: ga
};