var utils = require('./utils');
var viewUtilFns = require('./view-utils');
var PubSub = require('./pubsub');

require("../css/game.css");
var html = require('../html/game.partial.html');

var GameView = function(el){
  "use strict";
  var view = this;
  view.el = el || document.createElement('div');
  view.el.className = 'page game-view';
  view.states = ['init', 'playing', 'solved', 'unsolved'];
  view.el.innerHTML = html;

  view.play = function(game){
    view.updateScore(game);
    view.showMessage('Start Guessing...');
    view.updateInput();
    view.changeState('playing');
    view.animate(null, 'fadeIn');
    view.focusInput();
  };

  view.updateScore = function(game) {
    //update score
    var range = game.getRange();
    var input = view.el.querySelector('input[name="guess-number"]');
    input.setAttribute('min', range[0]);
    input.setAttribute('max', range[1]);
    view.el.querySelector('.range-info__value').innerHTML = range.join(' - ');
    view.el.querySelector('.attempt-info__value').innerHTML = game.getGuessCount() + '/' + game.optimumAttempts();
    view.el.querySelector('.attempt-info__value').classList[ ( game.getGuessCount()/game.optimumAttempts() >= 1) ? 'add' : 'remove'  ]('crossed-optimum');
  };

  view.showMessage = function(message){
    view.el.querySelector('.guess-message').innerHTML = message;
  };

  view.updateInput = function(value){
    view.el.querySelector('input[name="guess-number"]').value = value;
  };

  view.focusInput = function(){
    view.el.querySelector('input[name="guess-number"]').focus();
  };

  view.el.querySelector('.show-instructions').addEventListener('click', function(ev){
    view.publish('show-instructions', null);
  });

  view.el.querySelector('button[name="play-start"]').addEventListener('click', function(ev){
    view.publish('play-start', null);
  });

  view.el.querySelector('button[name="play-resume"]').addEventListener('click', function(ev){
    view.publish('play-resume', null);
  });

  view.el.querySelector('form[name="guess-form"]').addEventListener('submit', function(ev){
    var inputEl = ev.target.querySelector('input[name="guess-number"]'),
      number = parseInt(inputEl.value, 10);
    view.publish('guess-form-submit', number);
  });

  view.el.querySelector('button[name="play-again"]').addEventListener('click', function(ev){
    view.publish('play-again', ev);
  });

  view.el.querySelector('button[name="play-next"]').addEventListener('click', function(ev){
    view.publish('play-next', ev);
  });

  view.el.querySelector('button[name="give-up"]').addEventListener('click', function(ev){
    view.publish('play-giveup', ev);
  });

  view.changeState('init');
  view.animate(null, 'fadeIn');

};

GameView.prototype = utils.extend(GameView.prototype, viewUtilFns);
GameView.prototype = utils.extend(GameView.prototype, PubSub.prototype);

module.exports = GameView;