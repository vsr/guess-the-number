var utils = require('./utils');

module.exports = function GuessGame(min, max){
  "use strict";
  var minRange = min || 1,
    maxRange = max || 10,
    randomNumber,
    guesses = [],
    solved = false,
    inProgress = true,
    gaveUp = false,
    optimumAttempts = Math.floor(Math.pow(maxRange-minRange, 1/2))+1;

  randomNumber = utils.getRandomInt(minRange, maxRange);

  this.guess = function(number){
    if(!inProgress){ throw new Error('Game is over'); }
    if(isNaN(number)){ throw new Error('You need to guess a number!'); }
    guesses.push(number);
    if(number===randomNumber){
      solved = true;
      inProgress = false;
      return 0;
    }
    else{
      return (number-randomNumber)/Math.abs(number-randomNumber);
    }
  };

  this.giveUp = function(){
    if(inProgress){
      gaveUp = true;
      inProgress = false;
    }
    return randomNumber;
  };

  this.getRange = function(){ return [minRange, maxRange]; };
  this.optimumAttempts = function(){ return optimumAttempts; };

  this.getGuesses = function(){ return guesses.slice(); };
  this.getGuessCount = function(){ return guesses.length; };

  this.isSolved = function(){ return solved; };
  this.isInProgress = function(){ return inProgress; };

};