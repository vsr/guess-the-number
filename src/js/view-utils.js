module.exports = (function(){
  "use strict";
  var fns = {};

  fns.hide = function(cb){
    this.el.classList.add('hidden');
  };

  fns.show = function(cb){
    this.el.classList.remove('hidden');
  };

  fns.changeState = function(stateName){
    var prefix = 'state-';
    for(var i=0,j=this.states.length;i<j;i++){
        this.el.classList.remove(prefix+this.states[i]);
    }
    this.el.classList.add(prefix+stateName);
  };

  fns.animate = function(selector, animationClass){
    var el = selector ? this.el.querySelector(selector) : this.el;
    if(!el.classList.contains('animated')) { el.classList.add('animated'); }

    var fnCallback = function() {
      el.classList.remove(animationClass);
      el.removeEventListener('animationend', fnCallback);
    };
    el.addEventListener('animationend', fnCallback);
    el.classList.add(animationClass);
  };

  return fns;
}());
