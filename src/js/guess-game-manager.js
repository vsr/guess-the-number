var utils = require('./utils');
var GuessGame = require('./guess-game');


function GuessGameManager(level){
  "use strict";
  var games = [],
    game;

  level = level || 1;
  var minRange = 1,
    maxRange = (utils.fibonacci(level+3));

  function createNewGame(){
    game = new GuessGame(minRange, maxRange);
    games.push(game);
    return game;
  }
  function getGame(){
    if(!game){
      createNewGame();
    }
    return game;
  }

  function replayGame(){
    return createNewGame();
  }

  function nextRange(){
    return [minRange, utils.fibonacci(level+3)];
  }

  function moveNextLevel(){
    if(!game.isSolved()){
      throw new Error("Solve current level to move to next level");
    }

    level++;
    var next = nextRange();
    minRange = next[0];
    maxRange = next[1];
    createNewGame();
  }


  this.getGame = getGame;
  this.replayGame = replayGame;
  this.moveNextLevel = moveNextLevel;
  this.getRange = function(){ return [minRange, maxRange]; };
  this.getGames = function(){ return games; };
  this.getLevel = function(){ return level; };

}

GuessGameManager.prototype.messages = {
  guessStart: "Start guessing..."
};

module.exports = GuessGameManager;