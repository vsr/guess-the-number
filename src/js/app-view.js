var utils = require('./utils');
var viewUtilFns = require('./view-utils'),
    GameView = require('./game-view'),
    InstructionsView = require('./instructions-view');

require("../css/app.css");

var AppView = function(el){
  "use strict";
  var appView = this;
  appView.el = el || document.createElement('div');
  appView.states = ['init', 'playing', 'solved', 'unsolved'];
  appView.gameView = new GameView();
  appView.instructionsView = new InstructionsView();
  appView.el.appendChild(appView.gameView.el);
  appView.el.appendChild(appView.instructionsView.el);

  appView.showGame();
};

AppView.prototype = utils.extend(AppView.prototype, {
  showGame: function(animated){
    var view = this;
    view.instructionsView.animate(null, 'fadeOut');
    view.instructionsView.hide();
    view.gameView.show();
    view.gameView.animate(null, 'fadeIn');
  },
  showInstructions: function(animated){
    var view = this;
    view.gameView.animate(null, 'fadeOut');
    view.gameView.hide();
    view.instructionsView.show();
    view.instructionsView.animate(null, 'fadeIn');
  }
});

AppView.prototype = utils.extend(AppView.prototype, viewUtilFns);

module.exports = AppView;