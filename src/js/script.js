require("../css/style.css");

var GuessGameManager = require('./guess-game-manager');
var AppView = require('./app-view');
var utils = require('./utils');

function init () {
  'use strict';

  var level = 1;
  var appView = new AppView(document.querySelector('#stage')),
    gameManager;

  appView.gameView.subscribe('play-start', function(){
    gameManager = new GuessGameManager(level);
    appView.gameView.play(gameManager.getGame());
    appView.changeState('playing');
    utils.ga('send', 'event', 'Game', 'start');
  });

  appView.gameView.subscribe('play-resume', function(){
    if(window.localStorage && window.localStorage.getItem && window.localStorage.getItem('level')){
      level = Number(window.localStorage.getItem('level'));
    }
    gameManager = new GuessGameManager(level);
    appView.gameView.play(gameManager.getGame());
    appView.changeState('playing');
    utils.ga('send', 'event', 'Game', 'resume');
  });

  appView.gameView.subscribe('show-instructions', function(){
    appView.showInstructions();
    utils.ga('send', 'event', 'Instructions', 'view');
  });
  appView.instructionsView.subscribe('back-to-game', function(){
    appView.showGame();
    utils.ga('send', 'event', 'Instructions', 'close');
  });

  appView.gameView.subscribe('guess-form-submit', function(name, number){
    var result, message;
    try{
      result = gameManager.getGame().guess(number);
    }
    catch (e){
      console.error(e);
      appView.gameView.showMessage(e.message);
      appView.gameView.animate('.guess-message', 'pulse');
      utils.ga('send', 'event', 'Game', 'error', e.message);
      return;
    }
    if(result===0){
      appView.gameView.changeState('solved');
      appView.gameView.animate('form[name="game-off-options"]', 'fadeIn');
      appView.gameView.updateScore(gameManager.getGame());
      appView.gameView.showMessage('Your guess, '+number+', is correct!');
      appView.gameView.animate('.guess-message', 'pulse');
      appView.changeState('solved');
      utils.ga('send', 'event', 'Game', 'solved', gameManager.getRange().join('-'));
    }
    else {
      if(result>0){
        message = 'Your guess is high!';
      }
      else {
        message = 'Your guess is low!';
      }
      appView.gameView.updateScore(gameManager.getGame());
      appView.gameView.showMessage(message);
      appView.gameView.animate('.guess-message', 'wobble');
      appView.gameView.focusInput();
    }

  });

  appView.gameView.subscribe('play-again', function(name, ev){
    gameManager.replayGame();
    appView.gameView.play(gameManager.getGame());
    appView.changeState('playing');
    utils.ga('send', 'event', 'Game', 'replay', gameManager.getRange().join('-'));
  });

  appView.gameView.subscribe('play-next', function(name, ev){
    gameManager.moveNextLevel();
    appView.gameView.play(gameManager.getGame());
    appView.changeState('playing');
    utils.ga('send', 'event', 'Game', 'next', gameManager.getRange().join('-'));

  if(window.localStorage && window.localStorage.setItem){
    window.localStorage.setItem('level', gameManager.getLevel());
  }

  });

  appView.gameView.subscribe('play-giveup', function(name, ev){
    var number = gameManager.getGame().giveUp();
    appView.gameView.updateScore(gameManager.getGame());
    appView.gameView.showMessage('Correct number was '+ number+'.');
    appView.gameView.updateInput();
    appView.gameView.changeState('unsolved');
    appView.gameView.animate('.guess-message', 'pulse');
    appView.changeState('unsolved');
    utils.ga('send', 'event', 'Game', 'gaveup', gameManager.getRange().join('-'));
  });


}


init();
require('offline-plugin/runtime').install();