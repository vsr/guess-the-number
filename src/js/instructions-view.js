var utils = require('./utils');
var viewUtilFns = require('./view-utils');
var PubSub = require('./pubsub');
require("../css/instructions.css");
var html = require('../html/instructions.partial.html');

var InstructionsView = function(el){
  "use strict";
  var view = this;
  view.el = el || document.createElement('div');
  view.el.className = 'page instructions-view';
  view.el.innerHTML = html;

  view.el.querySelector('button.back-to-game').addEventListener('click', function(ev){
    view.publish('back-to-game', null);
  });

};

InstructionsView.prototype = utils.extend(InstructionsView.prototype, viewUtilFns);
InstructionsView.prototype = utils.extend(InstructionsView.prototype, PubSub.prototype);

module.exports = InstructionsView;
