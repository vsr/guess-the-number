
function PubSub(name){
    this.name = name || '';
}

PubSub.prototype.subscribe = function(event, fn){
    var me = this;
    event = String(event);
    if(!me._events) { me._events = {}; }
    if(!me._events[event]) { me._events[event] = []; }
    var index = me._events[event].push(fn) - 1;
    return (function(obj, index){
        return function(){ obj.splice(index, 1); };
    }(me._events[event], index));
};

PubSub.prototype.unsubscribeAll = function(event){
    var me = this;
    event = String(event);
    if(me._events && me._events[event]){
        me._events[event] = [];
    }
};

PubSub.prototype.publish = function(event, data){
    var me = this,
        i, j, listeners;
    event = String(event);
    if(me._events && me._events[event]){
        listeners = me._events[event];
        for(i=0,j=listeners.length; i<j; i++) {
            if(typeof listeners[i]==='function'){
                listeners[i](event, data);
            }
        }
    }
};

module.exports = PubSub;