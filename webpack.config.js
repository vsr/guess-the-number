require('es6-promise').polyfill();

var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CleanWebpackPlugin = require('clean-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var OfflinePlugin = require('offline-plugin');

var conf = require('./conf');

var mode = process.env.npm_lifecycle_event;
var plugins = [];

if(mode==='build'){
    plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    );
    plugins.push(
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            inject: false,
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                conservativeCollapse: true,
                preserveLineBreaks: true
            },
            mode: mode,
            conf: conf
        })
    );
    plugins.push(
        new OfflinePlugin({
            excludes: ['40x.html', '50x.html', 'img/*', 'script.bundle.js', 'main.css']
        })
    );

}
else if(mode==='dev'){

    plugins.push(
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            inject: true,
            minify: false,
            mode: mode,
            conf: conf
        })
    );

}

plugins.push(
 new CleanWebpackPlugin(['build'], {
        root: '',
        verbose: true,
        dry: false
    })
);

plugins.push(
    new ExtractTextPlugin("[name].css")
);

plugins.push(
    new CopyWebpackPlugin([
            {"from": "src/img/", "to": "img"},
            {"from": "src/manifest.json"},
            {"from": "src/favicon.ico"},
            {"from": "src/40x.html"},
            {"from": "src/50x.html"}
        ])
);


module.exports = {
    entry: './src/js/script.js',
    output: {
        path: './build/',
        filename: 'script.bundle.js'
    },
    module: {
        preLoaders: [
            { test: /\.js$/, loader: "jshint-loader", "exclude": /node_modules/ }
        ],
        loaders: [
            { test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") },
            { test: /\.partial\.html$/, loader: 'html' }
        ]
    },
    plugins: plugins
 };