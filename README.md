# Guess The Number

Guess The Number is a game of guessing the correct number. The aim of the game is to guess the correct number in as few attempts as possible.

Play online at http://guessthenumber.appb.in

### Building
```sh
$ npm install
$ npm run build
```
